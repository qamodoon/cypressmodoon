FROM cypress/included:10.3.0
LABEL maintainer="Danny Barrientos <dbarrientos@serempre.com>"
RUN mkdir /cypress-wqa
WORKDIR /cypress-wqa
COPY ./package.json .
COPY ./reporter-config.json .
COPY ./cypress.config.ts .
COPY ./jsconfig.json .
COPY ./tsconfig.json .
COPY ./cypress ./cypress
RUN npm install
ENTRYPOINT ["npm", "run"]