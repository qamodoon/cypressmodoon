/// <reference types="cypress" />


describe('Test for Tap it Webpage', () => {

  Cypress.on('uncaught:exception', (err, runnable) => {
   
   return false
  })
  
  beforeEach(() => {
    cy.visit('https://abinbev-co-modo-on-web-dev.serempre.dev/')
    cy.wait(800);
    cy.get("#onetrust-accept-btn-handler").click();
    cy.get('.cmtEsE').click();
    cy.wait(800)
    cy.get('.AlertDialogLocationstyles__StyledButtonsContainer-sc-muokko-3 > :nth-child(1)').click();
    
  })

  it('Log in con Boton INGRESA', () => {

    cy.get('[class="NavBtnLinkstyles__StyledBtnLabel-sc-8n7krx-1 PCwbJ"]').click();
    cy.get("#email").type("ngutierrez@serempre.com");
    cy.get('.ng-invalid > .sso-button').click();
    cy.get("#password").type("N4t4l141402!");
    cy.get('.ng-submitted > .sso-button').click();
    cy.wait(1600)
    cy.get('[class="NavProfile__StyledName-sc-fnyv8t-1 ewKbEQ"]')
      .should('contain', 'Natalia');
    cy.wait(1000)
    cy.get('[class="lg:hidden"]').click();
    cy.get('.NavMenustyles__StyledMenu-sc-1hja4u8-0 > [href="/"]').click({ force: true })
    cy.wait(400)
    cy.get('[class="NavCloseBtnstyles__StyledCloseBtn-sc-1gbvo4k-0 iVVIqy"]').click();
    cy.wait(1000)
    cy.get(".NavBtnLinkstyles__StyledBtnLabel-sc-8n7krx-1")
      .should('contain', 'Ingresa');
    cy.wait(1000)
    

});

  it('Recuperar Contraseña', () => {

    cy.get('[class="NavBtnLinkstyles__StyledBtnLabel-sc-8n7krx-1 PCwbJ"]').click();
    cy.get("#email").type("modoon_07qa@yopmail.com");
    cy.get('.ng-invalid > .sso-button').click();
    cy.get('.sso-container > .sso-text-primary-500').click();
    cy.wait(200)
    cy.get('.sso-button').click();
    cy.wait(600)
    cy.get(".sso-font-bold")
      .should('contain', '¡Correo enviado!');
      cy.get('.sso-right-0 > .uil').click();

  });

  it('Registro sin términos y condiciones', () => {

    cy.get('[class="NavBtnLinkstyles__StyledBtnLabel-sc-8n7krx-1 PCwbJ"]').click();
    cy.get("#email").type("qa_stella2@yopmail.com");
    cy.get(".ng-invalid > .sso-button").click()
    cy.wait(700);
    cy.get(".sso-text-lg")
      .should('contain', '¡Bienvenido Beer Lover!');
    cy.get("#firstName").type("Qa");
    cy.get("#lastName").type("Prueba");
    cy.get("#day").type("24");
    cy.get("#month").type("05");
    cy.get("#year").type("1994");
    cy.get('#mat-select-value-1').click();
    cy.get('#mat-option-1').click();
    cy.get("#password").type("Prueba12345");
    cy.get("#passwordVerification").type("Prueba12345");
    cy.get('.sso-button').click()
    cy.get(":nth-child(3) > .sso-error-hint")
    .should('contain', ' Debes aceptar los Términos y Condiciones para accedder ');
    cy.wait(500);

  });

  it('Login with three lines button', () => {
    
    cy.get('[class="lg:hidden"]').click();
    cy.get('[class="Buttonstyles__StyledButton-sc-khuhic-0 gfjqVC uppercase"]').click();
    cy.wait(700);
    cy.get("#email").type("ngutierrez@serempre.com");
    cy.get('.ng-invalid > .sso-button').click();
    cy.get("#password").type("N4t4l141402!");
    cy.get('.ng-submitted > .sso-button').click();
    cy.wait(1600)
    cy.get('[class="NavProfile__StyledName-sc-fnyv8t-1 ewKbEQ"]')
      .should('contain', 'Natalia');
    cy.wait(1000)
    cy.get('.NavMenustyles__StyledMenu-sc-1hja4u8-0 > [href="/"]').click({ force: true })
    cy.wait(400)
    cy.get('[class="NavCloseBtnstyles__StyledCloseBtn-sc-1gbvo4k-0 iVVIqy"]').click();
    cy.wait(1000)
    cy.get(".NavBtnLinkstyles__StyledBtnLabel-sc-8n7krx-1")
      .should('contain', 'Ingresa');
    cy.wait(1000)

});

it('Redimir cupon', () => {

  cy.get('[class="NavBtnLinkstyles__StyledBtnLabel-sc-8n7krx-1 PCwbJ"]').click();
  cy.get("#email").type("ngutierrez@serempre.com");
  cy.get('.ng-invalid > .sso-button').click();
  cy.get("#password").type("N4t4l141402!");
  cy.get('.ng-submitted > .sso-button').click();
  cy.wait(6000)
  cy.get(".NavProfile__StyledName-sc-fnyv8t-1 > p")
    .should('contain', 'Natalia');
  cy.wait(500)
  cy.get('[class="slick-slider Sliderstyles__StyledSlider-sc-1xtgo6x-0 giBduY slick-initialized"]').click();
  cy.wait(500)
  cy.get('[class="bg-white p-2 rounded z-20"]').click();
  cy.wait(300)
  cy.get('[class="lg:hidden"]').click();
  cy.get('.NavMenustyles__StyledMenu-sc-1hja4u8-0 > [href="/"]').click({ force: true })
  
});

});