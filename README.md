# cypress-modo-On-framework



### 💻 Topics

We reviewed topics like:

- [x] Mocha Structure
- [x] Mocha Hooks
- [x] Retry Ability

 
- ## 💻 Pre-requisites

Before you use this project you only need to have Node Js installed in your computer.

https://nodejs.org/es/download/

## 🚀 Install the project

Install project dependencies with: npm i 
